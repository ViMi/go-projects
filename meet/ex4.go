package main

import "fmt"

func main() {
	//Инициализация переменных
	var a int16 = 2
	var b int16 = 3
	var c int64 = 10

	fmt.Println("a + b        = ", a+b)
	fmt.Println("a - b        = ", a-b)
	fmt.Println("a * b        = ", a*b)
	fmt.Println("int(a / b)   = ", int(a/b), "\n")

	c--
	fmt.Println("c--     = ", c)
	c++
	fmt.Println("c++     = ", c)
	c += 10
	fmt.Println("c += 10 = ", c)
	c -= 5
	fmt.Println("c -= 5  = ", c)
	c *= 3
	fmt.Println("c *= 3  = ", c)
	c /= 7
	fmt.Println("c /= 7  = ", c)

	//Задание.
	//1. Выпонить выражение: fmt.Println("c--     = ", c--)

	//fmt.Println("C--   = ", c--) Выполнение в лоб не прокатит так как выбивает ошибку вероятно по причине того что
	// из языка убрали декремент как арифм. операцию допустимую в выводе для препятсвованию обфускации кода
	//(наверное, я бы сделал так, а то исходники на С иногда читать не возможно)
	//fmt.Println("Test C", c+=1) Так как операции даже такие незя
	c--
	c=10
	c-=1
	fmt.Println("C-- (9) = ", c)

}