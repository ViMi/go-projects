package main

import "fmt"

func main() {
	//Инициализация переменных
	var userinit8 uint8 = 1
	var userinit16 uint16 = 2
	var userinit64 int64 = -3
	var userautoinit = -4 //Такой вариант инициализации также возможен

	fmt.Println("Values: ", userinit8, userinit16, userinit64, userautoinit, "\n")


	//Краткая запись объявления переменной
	//только для новых переменных
	intVar := 10

	fmt.Printf("Value = %d Type = %T\n", intVar, intVar) //Интересная штука

	//Задание.
	//1. Вывести типы всех переменных
	fmt.Printf(
		"Type int8 = %T : Type int16= %T : Type int64 = %T : Type int = %T \n" ,
		userinit8, userinit16, userinit64, userautoinit)
	//Теперь возникает ворпос в С# есть объекты для форматирования
	//А тут отдельный метод, и язык вызывает вопросы касательно того что очень непревычно
	//Очень напоминает С в своем подходе к игнорированию объектов как таковых
	//Вероятно это сделано для упращения языка и оптимизации, но непревычно


	//2. Присвоить переменной intVar переменные userinit16 и userautoinit. Результат вывести.
	intVar = int(userinit16) //Привод типов странный
	fmt.Println(intVar)

	intVar = userautoinit
	fmt.Println(intVar)
}



