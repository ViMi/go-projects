package main

import "fmt"

func main() {
	variable8 := int8(127)
	variable16 := int16(16383)

	fmt.Println("Приведение типов\n")

	fmt.Printf("variable8         = %-5d = %.16b\n", variable8, variable8)
	fmt.Printf("variable16        = %-5d = %.16b\n", variable16, variable16)
	fmt.Printf("uint16(variable8) = %-5d = %.16b\n", uint16(variable8), uint16(variable8))
	fmt.Printf("uint8(variable16) = %-5d = %.16b\n", uint8(variable16), uint8(variable16))

	//Задание.
	//1. Создайте 2 переменные  разных типов. Выпоните арифметические операции. Результат вывести
	var x0 int8 = 125
	var x1 int16 = 252

	var x2 int32 = int32(int16(x0)-x1)
	//Хм... Замороченная система типов

	//var test1 int32 = 12
	//var test2 int16 = 16
	//var res int32  = (test1 + int32(test2))

	fmt.Println(x2)

	x2 = (int32(int16(x0)-x1) + int32(x1)) * int32(x0) //Как по мне не очень уданчная стратегия обработки прведения типв
	// Очень громоздко... В плюсах с кастами конечно еще интересней но они и нужны не так часто а тут как то перебор
	//Что интересно в С# тоже есть ограничения на приведение типов, но не настолько жесткие
	//С одной стороны сложнее сделать что-то не то, а сдругой раздражает
	fmt.Println(x2)



}
