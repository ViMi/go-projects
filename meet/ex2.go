package main

import "fmt"
import "math"

func main() {
	//Все переменные инициализируются значением по умолчанию, т.е. 0
	var defaultInt8 int8
	var defaultInt16 int16
	var defaultInt32 int32
	var defaultInt64 int64
	var defaultInt int

	fmt.Println("Default values (signed): ", defaultInt8, defaultInt16, defaultInt32, defaultInt64, defaultInt)

	var defaultuInt8 uint8
	var defaultuInt16 uint16
	var defaultuInt32 uint32
	var defaultuInt64 uint64
	var defaultuInt uint

	fmt.Println("Default values (unsigned): ", defaultuInt8, defaultuInt16, defaultuInt32, defaultuInt64, defaultuInt)

	//Задание.
	//1. Создать целочисленную переменную (результат не отображать)
	var taskVar int8 = 10
	var taskVar2 int16 = 12
	var taskVar3 int32 = 14
	var taskVar1Max int8 = math.MaxInt8
	var Something uint = math.MaxUint32

	fmt.Println(taskVar, "---" ,taskVar2, "---" , taskVar3, "--- ",taskVar1Max, " --- ",Something)
	//Теперь еще больше похоже на Python единнственное как по мне не очень логично что за вывод и ввод отвечает пакет "fmt"
	//Если не брать в рассчет вопросы производительности я бы сделал что пакет например console
	//Содержит все для ввода и вівода с консоли а в пакете format находяться объекты стратегии для форматирования данных
	//Или же сделал более интуитивное название пакета. Хотя я пока что не знаю яык в достаточной мере
	//Возможно такое название и будет логичным позденее
}
