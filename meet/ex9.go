package main

import "fmt"

func main() {

	var first, second bool
	var third bool = true
	fourth := !third
	var fifth = true


	fmt.Println("first  = ", first)    // false --> Выводит ЛОЖЬ так как это значении инициализации по умолчанию как в шарпе
	fmt.Println("second = ", second)   // false --> Выводит (смотреть прошлый анализ)
	fmt.Println("third  = ", third)    // true --> Выводити ИСТИНА так как оно присвоено
	fmt.Println("fourth = ", fourth)   // false --> Принимаее отричацие от ИСТИНА которое является ЛОЖЬ
	fmt.Println("fifth  = ", fifth, "\n") // true --> Выводит присвоенное зачение ИСТИНА

	fmt.Println("!true  = ", !true)        // false --> отрицание ИСТИНА == ЛОЖЬ
	fmt.Println("!false = ", !false, "\n") // true --> Отрицание ЛОЖЬ == ИСТИНА

	fmt.Println("true && true   = ", true && true)         // true -->
	// ДАННОЕ ВЫРАЖЕНИЕ ИСТИНА только когда оба операнда ИСТИНА (смотреть: любой адекватный язык)
	fmt.Println("true && false  = ", true && false)        // false --> (смотреть прошлый анализ)
	fmt.Println("false && false = ", false && false, "\n") // false --> (смотерть прошлый анализ)

	fmt.Println("true || true   = ", true || true)         // true --> Данное выражение ИСТИНА когда один из операндов истина
	// (смореть: любой адекватный язык)
	fmt.Println("true || false  = ", true || false)        // true --> (смотреть прошлы анализ)
	fmt.Println("false || false = ", false || false, "\n") // false --> (смотреть прошлы анализ)

	fmt.Println("2 < 3  = ", 2 < 3)        // true --> 2 меньше 3 это ИСТИНА
	fmt.Println("2 > 3  = ", 2 > 3)        // false --> 2 больше 3 это ЛОЖЬ
	fmt.Println("3 < 3  = ", 3 < 3)        // false --> 3 меньше 3 это ЛОЖЬ
	fmt.Println("3 <= 3 = ", 3 <= 3)       // true --> 3 меньше равно 3 это ИСТИНА
	fmt.Println("3 > 3  = ", 3 > 3)        // false --> 3 больше 3 это ЛОЖЬ
	fmt.Println("3 >= 3 = ", 3 >= 3)       // true --> 3 больше равно 3 это ИСТИНА
	fmt.Println("2 == 3 = ", 2 == 3)       // false --> 2 равно 3 это ЛОЖЬ
	fmt.Println("3 == 3 = ", 3 == 3)       // true --> 3 равно 3 это ИСТИНА
	fmt.Println("2 != 3 = ", 2 != 3)       // true --> 2 не равно 3 это ИСТИНА
	fmt.Println("3 != 3 = ", 3 != 3, "\n") // false --> 3 не равно 3 это ЛОЖЬ

	//Задание.
	//1.Пояснить результаты операций
		//Объяснения приложены слева от знака -->
}
