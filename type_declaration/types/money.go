package types

import "strconv"

//package Money
type Money struct {
	Main int64
	Coins int64
}

func NewMoney(main int64, coins int64) Money {
	return Money{
		Main: main,
		Coins : coins,
	}
}

func (leftValue Money) MullInt(rightValue int64,  maxCoinsPesMain int64) Money {
	main := leftValue.Main * rightValue + (int64((leftValue.Coins * rightValue)/maxCoinsPesMain))
	coins:= (leftValue.Coins * rightValue)%maxCoinsPesMain

	return Money{
		Main : main,
		Coins : coins,
	}
}


func (leftValue Money) Mull(rightValue Money, maxCoinsPesMain int64) Money {
	main := leftValue.Main * rightValue.Main + (int64((leftValue.Coins * rightValue.Coins)/maxCoinsPesMain))
	coins:= (leftValue.Coins * rightValue.Coins)%maxCoinsPesMain

	return Money{
		Main : main,
		Coins : coins,
	}
}


func(leftValue Money) Plus(rightValue Money, maxCoinsPesMain int64) Money {

	main := leftValue.Main + rightValue.Main + int64((leftValue.Coins + rightValue.Coins)/maxCoinsPesMain)
	coins := (leftValue.Coins + rightValue.Coins)%maxCoinsPesMain

	return Money{
		Main: main,
		Coins: coins,
	}
}


func(m* Money) ToString() string {
	var str string = ""
	str += "Money[Main, Coins] " + strconv.Itoa(int(m.Main)) + "." + strconv.Itoa(int(m.Coins))
	return str
}
