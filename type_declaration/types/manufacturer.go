package types

type Manufacturer struct {
	Name string
}

func NewManufacturer(name string) Manufacturer {
	return Manufacturer{
		Name: name,
	}
}

func(manuf* Manufacturer) ToString() string {
	var str string = ""
	str += "Manufacturer[Name] " + manuf.Name
	return str
}
