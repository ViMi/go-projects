package types

import "strconv"

type Product  struct {
	_name string // Name of product
	_price Money // Price of product
	_curr Currency //Currency name
	_quantity int64
	_producer Manufacturer
	_weight float32
}


func NewProduct(
	name string,
	price Money,
	cost Currency,
	quantity int64,
	producer Manufacturer,
	weight float32 ) Product {
	return  Product{
		_name: name,
		_price : price,
		_curr: cost,
		_quantity: quantity,
		_producer: producer,
		_weight : weight,
	}
}

func(prod* Product) SetName(name string) {
	prod._name = name
}

func (prod* Product) GetName() string {
	return prod._name
}


func(prod* Product) GetPrice() Money {
	return prod._price
}

func(prod* Product) SetPrice(price Money) {
	prod._price = price
}

func(prod* Product) SetCurrency(currency Currency) {
	prod._curr = currency
}

func(prod* Product) GetCurrency() Currency {
	return prod._curr
}


//Add ToString interface
func(prod* Product) ToString() string {
	var str string = ""
	str = "Price[Price] " + prod._price.ToString() + "\n"
	str += "Price[Currency] " + prod._curr.ToString() + "\n"
	str += "Price[Producer] " + prod._producer.ToString() + "\n"
	str += "Price[Name] " + prod._name + "\n"
	str += "Price[Weight] " + strconv.FormatFloat(float64(prod._weight), 'f', -1, 64) + "\n"
	str += "Price[Quantity]" + strconv.Itoa(int(int64(prod._quantity))) + "\n"
	return str
}


func(prod* Product) SetQuantity(quantity int64) {
	prod._quantity = quantity
}

func(prod* Product) GetQuantity() int64 {
	return prod._quantity
}

func(prod* Product) SetProducer(producer Manufacturer) {
	prod._producer = producer
}

func(prod* Product) GetProducer() Manufacturer {
	return prod._producer
}

func(prod* Product) SetWeight(weight float32) {
	prod._weight = weight
}

func(prod* Product) GetWeight() float32 {
	return prod._weight
}


func(prod* Product) GetPriceIn() Money {
	money := Money{}
	if prod._curr.Name != "UA"{
		money = prod._curr.ExRate.Mull(prod.GetPrice(), 100)
	} else {
		money = prod._price
	}

	return money
}

func (prod* Product) GetTotalPrice() Money {
	money := Money{}
	if prod._curr.Name != "UA"{
		money = (prod._curr.ExRate.Mull(prod.GetPrice(), 100)).MullInt(prod._quantity,100)
	} else {
		money = prod._price
	}

	return money
}


func(prod* Product) GetTotalWeight() float32 {
	return prod._weight * float32(prod._quantity)
}



