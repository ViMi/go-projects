package types
type Currency struct {
	Name string
	ExRate Money
}

func NewCurrency(name string, exRate Money) Currency {
	return Currency {
		Name: name,
		ExRate: exRate,
	}
}

func(curr* Currency) ToString() string {
	var str string = ""
	str += "Currency[Name, ExRate] " + curr.Name + " | " + curr.ExRate.ToString()
	return str
}

