package main

import (
	"fmt"
	"lab_5/types"
	"strconv"
)

var prod []types.Product

func ReadProductArray() {
	fmt.Println("How many products you want to enter ?")
	var prodN int32 = 0
	_, err :=fmt.Scanf("%d", &prodN)

	if err != nil {
		panic(err)
	}

	for i:=0; i < int(prodN); i++ {

		fmt.Println("["+strconv.Itoa(i)+"]")
		fmt.Println("-------------ENTER PRODUCT---------")

		var newProduct types.Product = types.Product{}
		var name string = ""
		var quantiny int64 = 0
		var weight float32 = .0

		fmt.Println("-[Name]-")
		fmt.Scanf("%s", &name)
		fmt.Println("-[Quantity]-")
		fmt.Scanf( "%d",&quantiny)
		fmt.Println("-[Weight]-")
		fmt.Scanf("%f", &weight)


		fmt.Println("------------Enter Currency ----")
		var currency types.Currency = types.Currency{}
		fmt.Println("-[Name]-")
		fmt.Scanf("%s", &currency.Name)
		fmt.Println("-[Main]-")
		fmt.Scanf("%d", &currency.ExRate.Main)
		fmt.Println("-[Coins]-")
		fmt.Scanf("%d", &currency.ExRate.Coins)

		fmt.Println("------------------Enter Product Price------------")
		var price types.Money = types.Money{}
		fmt.Println("-[Main]-")
		fmt.Scanf( "%d", &price.Main)
		fmt.Println("-[Coins]-")
		fmt.Scanf("%d", &price.Coins)


		fmt.Println("-----------------Enter Manufacturer------------")
		var manuf types.Manufacturer = types.Manufacturer{}
		fmt.Println("-[Name]-")
		fmt.Scanf("%s", &manuf.Name)


		newProduct.SetName(name)
		newProduct.SetQuantity(quantiny)
		newProduct.SetWeight(weight)

		newProduct.SetCurrency(currency)
		newProduct.SetProducer(manuf)
		newProduct.SetPrice(price)
		prod = append(prod, newProduct)
	}
}




func GetOptimum(prod []types.Product, comparor func(left types.Product, right types.Product) bool) types.Product {
	var optimum types.Product = prod[0]
	for _, tmp := range prod {
		if comparor(optimum, tmp) {
			optimum = tmp
		}
	}

	return optimum
}



func PrintProducts(prod []types.Product) {
	for _, value := range prod {
		PrintProduct(value)
	}
}



//Return maximum end minimum product in collection
func GetProductsOptimum(prod []types.Product) (types.Product, types.Product) {
	var minPriceProd types.Product = types.Product{}
	var maxPriceProd types.Product = types.Product{}

	minPriceProd = GetOptimum(prod, func(left types.Product, right types.Product) bool {
		if left.GetQuantity() < right.GetQuantity() {
			return true
		} else {
			return false
		}
	})

	maxPriceProd = GetOptimum(prod, func(left types.Product, right types.Product) bool {
		if left.GetQuantity() > right.GetQuantity() {
			return true
		} else {
			return false
		}
	})

	return minPriceProd, maxPriceProd
}



func PrintProduct(prod types.Product) {
	fmt.Println(prod.ToString())
}



func main() {


	//ReadProductArray
	ReadProductArray()

	//Print One Product
	fmt.Println("--------------DISPLAY FIRST PROD-------------")
	PrintProduct(prod[0])
	//Print All Product
	fmt.Println("--------------DISPLAY ALL PROD-------------")
	PrintProducts(prod)

	//GetProduct Optimum
	max, min := GetProductsOptimum(prod)

	fmt.Println("--------------MAX --- MIN-------------")
	fmt.Println(max.ToString())
	fmt.Println(min.ToString())

}
