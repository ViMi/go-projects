package main

import (
	"fmt"
	"lab3/genetic"
	"math"
	"math/rand"
	"time"
)

func main() {

	var N float64 = 1000000
	genetic.Configure(
		N,
		100,
		2000,
		8,

		func (x float64) float64 { // //Work Function
		return math.Sin(math.Pi * x / 180) - 1/x
	},

	func(args... float64) float64 { // Mutation Function
		rand.Seed(time.Now().UnixNano())
		return ((float64(int32(rand.Float64() * N) % int32((args[1] - args[0]) * N)) + 1) / N) + args[0]
	})




	///Call Genetic
	genetic.Genetic(0, 100, 0.1, func(param []float64) {
		for i := 0; i < len(param); i++ {
			fmt.Println(param[i]) // Callback
		}
	} )
}

