package genetic

import (
	"math"
	"math/rand"
	"time"
)

var N float64 = 1000000.

var MAX_POPULATION = 100

var ITER_MAX = 20000

var CROSSOVER_DiVIDE int = 8



var WORK_FUNC = func (x float64) float64 { // //Work Function
	return math.Sin(math.Pi * x / 180) - 1/x
}

var MUTATION_FUNC = func(args... float64) float64 { // Mutation Function
	rand.Seed(time.Now().UnixNano())
	return ((float64(int32(rand.Float64() * N) % int32((args[1] - args[0]) * N)) + 1) / N) + args[0]
}





func Configure(
	NValue float64,
	maxPopulation int,
	iterMax int,
	crossoverDivide int,
	workFunc func(x float64) float64,
	mutationFunc func(args... float64) float64) {


	N = NValue
	MAX_POPULATION = maxPopulation
	ITER_MAX = iterMax
	WORK_FUNC = workFunc
	CROSSOVER_DiVIDE = crossoverDivide
	MUTATION_FUNC = mutationFunc
}




func WorkFunction(x float64) float64 {return WORK_FUNC(x)}
func Mutation(x0 float64, x1 float64) float64 { return MUTATION_FUNC(x0, x1)}



func Inversion(x float64, eps float64, sign* int32) float64 {

	*sign++
	*sign %= 2

	if *sign == 0 {
		return x-eps
	} else {
		return x + eps
	}
}

func Crossover(x []float64, eps float64, x0 float64, x1 float64) {

	var k = MAX_POPULATION - 1

	for i := 0; i < CROSSOVER_DiVIDE; i++ {
		for j := i + 1; j < CROSSOVER_DiVIDE; j++ {
			x[k] = (x[i] + x[j]) / 2
			k--
		}
	}





	 var sign int32 = 0
	for i := 0; i < CROSSOVER_DiVIDE; i++ {
		x[k] = Inversion(x[i], eps, &sign)
		k--
		x[k] = Inversion(x[i], eps, &sign)
		k--
	}


	for i := CROSSOVER_DiVIDE; i < k; i++ {
		x[i] = Mutation(x0, x1)
	}
}



func Sort(x []float64, y []float64) {
	for i := 0; i < MAX_POPULATION; i++ {
	for j := i + 1; j < MAX_POPULATION; j++ {
		if math.Abs(y[j]) < math.Abs(y[i]) {
			temp := y[i]
			y[i] = y[j]
			y[j] = temp
			temp = x[i]
			x[i] = x[j]
			x[j] = temp
		}
	}
	}
}


func Genetic(x0 float64, x1 float64, eps float64, callback func([]float64)) float64 {

	if MAX_POPULATION == 0 || MAX_POPULATION < 0 {
		panic("Error")
	}


	var population []float64
	var f []float64

	var iter int = 0

	for i := 0; i < MAX_POPULATION; i++ {
		population = append(population, Mutation(x0,x1))
		f = append(f ,WorkFunction(population[i]))
	}

	Sort(population, f)


	for ; math.Abs(f[0]) > eps && iter < ITER_MAX;{
		iter++
		Crossover(population, eps, x0, x1)
		for i := 0; i < MAX_POPULATION; i++ {
			f[i] = WorkFunction(population[i])
		}

		Sort(population, f)
	}


	if callback != nil {
		callback(population)
	}

	return population[0]
}
