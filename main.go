package main



import _ "github.com/andlabs/ui/winmanifest" //Без этого не работает под виндой НЕ ТРОГАТЬ ! И НЕ ДЫШАТЬ В ЕГО СТОРОНУ
import (
	"fmt"
	"github.com/andlabs/ui"
	"strconv"
)

var _selectedCameras int = 1
var _isWindowsill bool = false
var _materialModify float64 = 1.1
var _baseWindowsillPriceModify float64 = 1.2

//Task One ---- God we trust
func InitGUI()  {

	title := "Window Task #1"
	width := 150
	height := 200

	window := ui.NewWindow(title, width, height, false)

	content := ui.NewHorizontalBox()
	labels := ui.NewVerticalBox()
	fields := ui.NewVerticalBox()

	content.SetPadded(true)
	labels.SetPadded(true)
	fields.SetPadded(true)


	//
	widthLabel := ui.NewLabel("Width")
	heightLabel := ui.NewLabel("Height")
	widthField := ui.NewEntry()
	heightField := ui.NewEntry()

	materialLabel := ui.NewLabel("Material")
	materialCombobox := ui.NewCombobox()
	materialCombobox.Append("Wood")
	materialCombobox.Append("Plastic")
	materialCombobox.Append("Iron")
	materialCombobox.SetSelected(0)

	resultLabel := ui.NewLabel("Cost-> ")


	glassPacketLabel := ui.NewLabel("Glass Packet")
	glassPacketCombobox := ui.NewCombobox()
	glassPacketCombobox.Append("One Cameras")
	glassPacketCombobox.Append("Two Cameras")
	glassPacketCombobox.SetSelected(0)

	windowsillChekcbox := ui.NewCheckbox("Windowsill")
	calculateButton := ui.NewButton("Calculate")


	labels.Append(widthLabel, true)
	labels.Append(heightLabel, true)
	labels.Append(materialLabel, true)
	labels.Append(glassPacketLabel, true)
	labels.Append(resultLabel, true)

	fields.Append(widthField, true)
	fields.Append(heightField, true)
	fields.Append(materialCombobox, true)
	fields.Append(glassPacketCombobox, true)
	fields.Append(windowsillChekcbox, true)
	fields.Append(calculateButton, true)

	content.Append(labels, true)
	content.Append(fields, true)

	window.SetChild(content)

	glassPacketCombobox.OnSelected(
		func (combobox *ui.Combobox) {
			_selectedCameras = combobox.Selected() + 1
		})

	windowsillChekcbox.OnToggled(
		func(checkbox *ui.Checkbox) {
			_isWindowsill = checkbox.Checked()
		})


	materialCombobox.OnSelected(
		func (combobox *ui.Combobox) {
			_materialModify = 1.
			_materialModify += float64(combobox.Selected()+1) * 0.1
		})

	calculateButton.OnClicked(
		func(button *ui.Button) {

			var res float64 = 0

			width, _ := strconv.Atoi(widthField.Text())
			height,_ := strconv.Atoi(heightField.Text())

			if _isWindowsill {
				res = float64(width * height) * _baseWindowsillPriceModify * float64(_selectedCameras) * _materialModify
			} else {
				res = float64(width * height) * float64(_selectedCameras) * _materialModify
			}

			resStr := fmt.Sprintf("%f", res )
			resultLabel.SetText("Cost -> " + resStr)

		})

	window.Show()

}




///----------------TASK 2
var _individualGuidePrice = 50.
var _premiumFlatPercentage = 1.2
var _prices = [6]float64{100, 150, 160, 200, 120, 180}

var _isIndividualGuide = false
var _isPremiumFlat = false

var _currentPrice = _prices[0]

func InitGUITask2() {

	title := "Window Task #2 [Tour]"
	width := 150
	height := 150

	window := ui.NewWindow(title, width, height, false)

	content := ui.NewHorizontalBox()
	labels := ui.NewVerticalBox()
	fields := ui.NewVerticalBox()

	content.SetPadded(false)
	labels.SetPadded(false)
	fields.SetPadded(false)

	//
	countryLabel := ui.NewLabel("Country")

	countryCombobox := ui.NewCombobox()
	countryCombobox.Append("Болгария, лето -> $100")
	countryCombobox.Append("Болгария, зима -> $150")
	countryCombobox.Append("Германия, лето -> $160")
	countryCombobox.Append("Германия, зима -> $200")
	countryCombobox.Append("Польша, лето -> $120")
	countryCombobox.Append("Польша, зима -> $180")
	countryCombobox.SetSelected(0)

	resultLabel := ui.NewLabel("Cost-> ")


	guideChekcbox := ui.NewCheckbox("Individual Guide")
	premiumFlatCheckBox := ui.NewCheckbox("Premium Flat")

	calculateButton := ui.NewButton("Calculate")


	labels.Append(countryLabel, false)
	labels.Append(ui.NewLabel(""), false)
	labels.Append(ui.NewLabel(""), false)
	labels.Append(resultLabel, false)

	fields.Append(countryCombobox, false)
	fields.Append(guideChekcbox, false)
	fields.Append(premiumFlatCheckBox, false)
	fields.Append(calculateButton, true)

	content.Append(labels, true)
	content.Append(fields, true)

	window.SetChild(content)


	premiumFlatCheckBox.OnToggled(
		func(checkbox *ui.Checkbox) {
			_isPremiumFlat = checkbox.Checked()
		})

	guideChekcbox.OnToggled(
		func(checkbox *ui.Checkbox) {
			_isIndividualGuide = checkbox.Checked()
		})


	countryCombobox.OnSelected(
		func (combobox *ui.Combobox) {
			_currentPrice = _prices[combobox.Selected()]
		})

	calculateButton.OnClicked(
		func(button *ui.Button) {

			var res float64 = 0

			if _isIndividualGuide {
				res += _individualGuidePrice + _currentPrice
			} else {
				res = _currentPrice
			}

			if _isPremiumFlat {
				res *= _premiumFlatPercentage
			}

			resStr := fmt.Sprintf("%f", res )
			resultLabel.SetText("Cost -> " + resStr)

		})

	window.Show()

}



//In nomine Domini Kernighan et Ritchie et #Pragma_Once. Amen
func main() {

	  ui.Main(InitGUI)
}

