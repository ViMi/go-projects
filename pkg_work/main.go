// main
package main

import (
	"2/pkg"
	"fmt"
	"github.com/logrusorgru/aurora"
	"gopkg.in/urfave/cli.v1"
	"net/http"
	"os"
	"time"
)

func main() {
	app := cli.NewApp()
	app.Name = "Lab Work Two"
	app.Usage = "That application present work with lab work two"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "task, t",
			Value: "Exercise num",
			Usage: "Execute app task",
		},
	}

	app.Action = func(c *cli.Context) error {
		fmt.Println(aurora.Yellow("[Hello user]"))
		var err error
		var array = []int32{1, 2, 3, 4, 5, 6, 7, 8, 9}

		//Что-то что а создание go-рутин и оглашение отложенных функций очень удобное
		defer (func() {if err != nil {fmt.Println(err)}})()

		go (func(params ...int32) {
				avg, _ := pkg.Average(params...)
				max, _ := pkg.Max(params...)
				res, _ := pkg.Resolve(10)

				message :=
					"<h1>Golang Server</h1> <br/>" +
						"----- AVERAGE " + fmt.Sprintf("%f",avg) + "<br/>" +
						"----- MAX " + fmt.Sprintf("%d",max) + "<br/>" +
						"----- RESOLVE " + fmt.Sprintf("%f", res) + "<br/>"


				http.HandleFunc(
					"/",
					func(w http.ResponseWriter, r *http.Request) {
						fmt.Fprintf(w, message)
					})

			http.ListenAndServe(":80", nil)

		})(array...)



		// для коректной работы с пакетами нужно было
		//или через командную строку или через встренный терминал JetBrains Go
		//Прописать go env -w GO111MODULE=off так как в противном случаии
		//возникала ошибка pakage is not in GOROOT
		//Как удалось выяснить поддержка модулей в виде который он есть сейчас возникла
		//В версии Го 1.11, а именоо проблема в том что
		//введенная переменная среды GO111MODULE которая отвечает за то как устанавливать пакеты
		//от версии к версии ведет себя по разному. хотя это не совсем мой случай.
		// я думая что данную особенность стоит использовать в лекцииях в качестве ремарки
		//Ссылка на источник
		//https://medium.com/@serene_ruby_caribou_28/%D0%BE%D1%82-gopath-%D0%B4%D0%BE-go111module-edbf3f9266e0



		//А вот обработка ошибок уже как по мне не очень удобна
		//Возврат нескольких значений отчасти конечно помогает ситуации
		//И возможно лучше подходить для оптимизации, но чисто субьективно использовани DTO мне как-то ближе
		//Хотя принудительная потребность писать вот так как ниже вероятно повыщает надежность программы

		fmt.Println(aurora.BgBrightWhite("--------------------------"))
		fmt.Println(aurora.BgBrightWhite(aurora.Black("average --- Calculate average values in 1,2,3,4,5,6,7,8,9")))
		fmt.Println(aurora.BgBrightWhite(aurora.Black("max --- Calculate max")))
		fmt.Println(aurora.BgBrightWhite(aurora.Black("resolve --- Calculate resolve")))
		fmt.Println(aurora.BgBrightWhite(aurora.Black("Server Starts Automatically (port 3030)// localhost:3030/lab_2_go_lang/ --- Server page")))
		fmt.Println(aurora.BgBrightWhite("--------------------------"))



		for {

			var task string =
				c.GlobalString("task")

			if task == "average" {


				average, err := pkg.Average(array...)
				if err != nil {return nil}

				fmt.Println(aurora.BgGreen(average))

				time.Sleep(5000)
				return nil

			} else if task == "max" {

				max, err := pkg.Max(array...)
				if err != nil {return nil}

				fmt.Println(aurora.BgYellow(max))

				time.Sleep(5000)
				return nil


			} else if task == "resolve" {

				solvation, err := pkg.Resolve(10)
				if err != nil {return nil}

				fmt.Println(aurora.BgMagenta(solvation))


				time.Sleep(5000)
				return nil

			} else if task == "exit" {

				return nil

			} else if task == "server " {

			} else {return nil}
		}

	}

	app.Run(os.Args)
}
