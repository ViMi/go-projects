package pkg

import "errors"

func Average(params ...int32) (float64, error) {

	if len(params) == 0 {
		return 1, errors.New("params array empty")
	}

	var tmp = params[0]
	var count = 1
	for i, _ := range params {
		count++
		tmp += params[i]
	}

	return float64(tmp)/float64(count), nil
}
