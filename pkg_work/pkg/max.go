package pkg

import "errors"

func Max(params ...int32) (int32, error) {

	if len(params) == 0 {
		return 1, errors.New("params array empty")
	}

	var max = params[0]
	for i, _:= range params {
		if params[i] > max {
			max = params[i]
		}
	}

	return max, nil
}